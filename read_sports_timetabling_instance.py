from xml.dom import minidom
from xml.dom.minidom import Node
from constraint_class import CA1, CA2, CA3, CA4, GA1, BR1, BR2, FA2, SE1

def handle_teams(mydoc,debug):
    teams = mydoc.getElementsByTagName('team')
    array_team_id = []
    array_team_league = []
    array_team_name = []

    # menampilkan jumlah team
    if debug == 1:
        print('Jumlah Team : {}'.format(len(teams)))

    # simpan value tiap atribut ke array
    for team in teams:
        # print(team.attributes['name'].value)
        array_team_id.append(team.attributes['id'].value)
        array_team_league.append(team.attributes['league'].value)
        array_team_name.append(team.attributes['name'].value)

    if debug == 1:
        print('array team id is :', array_team_id)
        print('array team league is :', array_team_league)
        print('array team name is :', array_team_name)

    return array_team_id

def handle_slots(mydoc,debug):
    slots = mydoc.getElementsByTagName('slot')
    array_slot_id = []
    array_slot_name = []

    # menampilkan jumlah slot
    if debug == 1:
        print('Jumlah Slot : {}'.format(len(slots)))

    # simpan value tiap atribut ke array
    for slot in slots:
        # print(slot.attributes['name'].value)
        array_slot_id.append(slot.attributes['id'].value)
        array_slot_name.append(slot.attributes['name'].value)

    if debug == 1:
        print('array slot id is :', array_slot_id)
        print('array slot name is :', array_slot_name)

    return array_slot_id

def handle_ca1(mydoc,debug):
    ca1 = mydoc.getElementsByTagName('CA1')
    ca1_temp = []

    # menampilkan jumlah constraint CA1
    if debug == 1:
        print('Jumlah CA1 : {}'.format(len(ca1)))

    # simpan value tiap atribut ke array
    for item in ca1:
        # print(slot.attributes['name'].value)
        ca1_temp.append( 
            CA1(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value.split(';'),
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    if debug == 1:
        for obj in ca1_temp:
            print(
                'max :', obj.max,
                ', min :', obj.min,
                ', mode :', obj.mode,
                ', penalty :', obj.penalty,
                ', slots :', obj.slots,
                ', teams :', obj.teams,
                ', type :', obj.type,
            sep =' ' )
    
    return ca1_temp

def handle_ca2(mydoc,debug):
    ca2 = mydoc.getElementsByTagName('CA2')
    ca2_temp = []

    # menampilkan jumlah constraint CA2
    if debug == 1:
        print('Jumlah CA2 : {}'.format(len(ca2)))

    # simpan value tiap atribut ke array
    for item in ca2:
        # print(slot.attributes['name'].value)
        ca2_temp.append( 
            CA2(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value.split(';'),
                item.attributes['teams1'].value.split(';'),
                item.attributes['teams2'].value.split(';'),
                item.attributes['type'].value,
            ) 
        )

    if debug == 1:
        for obj in ca2_temp:
            print(
                'max :', obj.max,
                ', min :', obj.min,
                ', mode1 :', obj.mode1,
                ', mode2 :', obj.mode2,
                ', penalty :', obj.penalty,
                ', slots :', obj.slots,
                ', teams1 :', obj.teams1,
                ', teams2 :', obj.teams2,
                ', type :', obj.type,
            sep =' ' )

    return ca2_temp

def handle_ca3(mydoc,debug):
    ca3 = mydoc.getElementsByTagName('CA3')
    ca3_temp = []

    # menampilkan jumlah constraint CA3
    if debug == 1:
        print('Jumlah CA3 : {}'.format(len(ca3)))

    # simpan value tiap atribut ke array
    for item in ca3:
        # print(slot.attributes['name'].value)
        ca3_temp.append( 
            CA3(
                item.attributes['intp'].value,
                item.attributes['max'].value,
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['teams1'].value.split(';'),
                item.attributes['teams2'].value.split(';'),
                item.attributes['type'].value,
            ) 
        )

    if debug == 1:
        for obj in ca3_temp:
            print(
                'intp :', obj.intp,
                ', max :', obj.max,
                ', min :', obj.min,
                ', mode1 :', obj.mode1,
                ', mode2 :', obj.mode2,
                ', penalty :', obj.penalty,
                ', teams1 :', obj.teams1,
                ', teams2 :', obj.teams2,
                ', type :', obj.type,
            sep =' ' )
    
    return ca3_temp

def handle_ca4(mydoc,debug):
    ca4 = mydoc.getElementsByTagName('CA4')
    ca4_temp = []

    # menampilkan jumlah constraint CA4
    if debug == 1:
        print('Jumlah CA4 : {}'.format(len(ca4)))

    # simpan value tiap atribut ke array
    for item in ca4:
        # print(slot.attributes['name'].value)
        ca4_temp.append( 
            CA4(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value.split(';'),
                item.attributes['teams1'].value.split(';'),
                item.attributes['teams2'].value.split(';'),
                item.attributes['type'].value,
            ) 
        )

    if debug == 1:
        for obj in ca4_temp:
            print(
                'max :', obj.max,
                ', min :', obj.min,
                ', mode1 :', obj.mode1,
                ', mode2 :', obj.mode2,
                ', penalty :', obj.penalty,
                ', slots :', obj.slots,
                ', teams1 :', obj.teams1,
                ', teams2 :', obj.teams2,
                ', type :', obj.type,
            sep =' ' )
    
    return ca4_temp

def handle_ga1(mydoc):
    ga1 = mydoc.getElementsByTagName('GA1')
    ga1_temp = []

    # menampilkan jumlah constraint GA1
    print('Jumlah GA1 : {}'.format(len(ga1)))

    # simpan value tiap atribut ke array
    for item in ga1:
        # print(slot.attributes['name'].value)
        ga1_temp.append( 
            GA1(
                item.attributes['max'].value,
                item.attributes['meetings'].value,
                item.attributes['min'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in ga1_temp:
        print(
            'max :', obj.max,
            ', meetings :', obj.meetings,
            ', min :', obj.min,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', type :', obj.type,
        sep =' ' )

def handle_br1(mydoc):
    br1 = mydoc.getElementsByTagName('BR1')
    br1_temp = []

    # menampilkan jumlah constraint BR1
    print('Jumlah BR1 : {}'.format(len(br1)))

    # simpan value tiap atribut ke array
    for item in br1:
        # print(slot.attributes['name'].value)
        br1_temp.append( 
            BR1(
                item.attributes['intp'].value, 
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in br1_temp:
        print(
            'intp :', obj.intp,
            ', mode1 :', obj.mode1,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_br2(mydoc):
    br2 = mydoc.getElementsByTagName('BR2')
    br2_temp = []

    # menampilkan jumlah constraint BR2
    print('Jumlah BR2 : {}'.format(len(br2)))

    # simpan value tiap atribut ke array
    for item in br2:
        # print(slot.attributes['name'].value)
        br2_temp.append( 
            BR2(
                item.attributes['intp'].value, 
                item.attributes['homeMode'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in br2_temp:
        print(
            'intp :', obj.intp,
            ', homeMode :', obj.homeMode,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_fa2(mydoc):
    fa2 = mydoc.getElementsByTagName('FA2')
    fa2_temp = []

    # menampilkan jumlah constraint FA2
    print('Jumlah FA2 : {}'.format(len(fa2)))

    # simpan value tiap atribut ke array
    for item in fa2:
        # print(slot.attributes['name'].value)
        fa2_temp.append( 
            FA2(
                item.attributes['intp'].value,
                item.attributes['mode'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in fa2_temp:
        print(
            'intp :', obj.intp,
            ', mode :', obj.mode,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_se1(mydoc):
    se1 = mydoc.getElementsByTagName('SE1')
    se1_temp = []

    # menampilkan jumlah constraint SE1
    print('Jumlah SE1 : {}'.format(len(se1)))

    # simpan value tiap atribut ke array
    for item in se1:
        # print(slot.attributes['name'].value)
        se1_temp.append( 
            SE1(
                item.attributes['mode1'].value, 
                item.attributes['min'].value,
                item.attributes['penalty'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in se1_temp:
        print(
            'mode1 :', obj.mode1,
            ', min :', obj.min,
            ', penalty :', obj.penalty,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )