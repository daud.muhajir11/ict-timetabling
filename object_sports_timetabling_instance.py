from xml.dom import minidom
from xml.dom.minidom import Node
from constraint_class import SOLUTION, TEAMS, SLOTS, CA1, CA2, CA3, CA4, GA1, BR1, BR2, FA2, SE1

def object_solution(solution):
    obj_solution = []
    # simpan value tiap atribut ke array
    for item in solution:
        obj_solution.append( 
            SOLUTION(
                item.attributes['home'].value, 
                item.attributes['away'].value,
                item.attributes['slot'].value,
            ) 
        )
    
    return obj_solution

def object_teams(mydoc):
    teams = mydoc.getElementsByTagName('team')
    obj_teams = []

    # simpan value tiap atribut ke array
    for item in teams:
        # print(slot.attributes['name'].value)
        obj_teams.append( 
            TEAMS(
                item.attributes['id'].value, 
                item.attributes['league'].value,
                item.attributes['name'].value,
            ) 
        )
    
    return obj_teams

def object_slots(mydoc):
    slots = mydoc.getElementsByTagName('slot')
    obj_slots = []

    # simpan value tiap atribut ke array
    for item in slots:
        # print(slot.attributes['name'].value)
        obj_slots.append( 
            SLOTS(
                item.attributes['id'].value,
                item.attributes['name'].value,
            ) 
        )
    
    return obj_slots

def object_ca1(mydoc):
    ca1 = mydoc.getElementsByTagName('CA1')
    obj_ca1 = []

    # simpan value tiap atribut ke array
    for item in ca1:
        obj_ca1.append( 
            CA1(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value.split(';'),
                item.attributes['teams'].value.split(';'),
                item.attributes['type'].value,
            ) 
        )

    return obj_ca1

def handle_ca2(mydoc):
    ca2 = mydoc.getElementsByTagName('CA2')
    ca2_temp = []

    # menampilkan jumlah constraint CA2
    print('Jumlah CA2 : {}'.format(len(ca2)))

    # simpan value tiap atribut ke array
    for item in ca2:
        # print(slot.attributes['name'].value)
        ca2_temp.append( 
            CA2(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams1'].value,
                item.attributes['teams2'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in ca2_temp:
        print(
            'max :', obj.max,
            ', min :', obj.min,
            ', mode1 :', obj.mode1,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams1 :', obj.teams1,
            ', teams2 :', obj.teams2,
            ', type :', obj.type,
        sep =' ' )

def handle_ca3(mydoc):
    ca3 = mydoc.getElementsByTagName('CA3')
    ca3_temp = []

    # menampilkan jumlah constraint CA3
    print('Jumlah CA3 : {}'.format(len(ca3)))

    # simpan value tiap atribut ke array
    for item in ca3:
        # print(slot.attributes['name'].value)
        ca3_temp.append( 
            CA3(
                item.attributes['intp'].value,
                item.attributes['max'].value,
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['teams1'].value,
                item.attributes['teams2'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in ca3_temp:
        print(
            'intp :', obj.intp,
            ', max :', obj.max,
            ', min :', obj.min,
            ', mode1 :', obj.mode1,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', teams1 :', obj.teams1,
            ', teams2 :', obj.teams2,
            ', type :', obj.type,
        sep =' ' )

def handle_ca4(mydoc):
    ca4 = mydoc.getElementsByTagName('CA4')
    ca4_temp = []

    # menampilkan jumlah constraint CA4
    print('Jumlah CA4 : {}'.format(len(ca4)))

    # simpan value tiap atribut ke array
    for item in ca4:
        # print(slot.attributes['name'].value)
        ca4_temp.append( 
            CA4(
                item.attributes['max'].value, 
                item.attributes['min'].value,
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams1'].value,
                item.attributes['teams2'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in ca4_temp:
        print(
            'max :', obj.max,
            ', min :', obj.min,
            ', mode1 :', obj.mode1,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams1 :', obj.teams1,
            ', teams2 :', obj.teams2,
            ', type :', obj.type,
        sep =' ' )

def handle_ga1(mydoc):
    ga1 = mydoc.getElementsByTagName('GA1')
    ga1_temp = []

    # menampilkan jumlah constraint GA1
    print('Jumlah GA1 : {}'.format(len(ga1)))

    # simpan value tiap atribut ke array
    for item in ga1:
        # print(slot.attributes['name'].value)
        ga1_temp.append( 
            GA1(
                item.attributes['max'].value,
                item.attributes['meetings'].value,
                item.attributes['min'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in ga1_temp:
        print(
            'max :', obj.max,
            ', meetings :', obj.meetings,
            ', min :', obj.min,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', type :', obj.type,
        sep =' ' )

def handle_br1(mydoc):
    br1 = mydoc.getElementsByTagName('BR1')
    br1_temp = []

    # menampilkan jumlah constraint BR1
    print('Jumlah BR1 : {}'.format(len(br1)))

    # simpan value tiap atribut ke array
    for item in br1:
        # print(slot.attributes['name'].value)
        br1_temp.append( 
            BR1(
                item.attributes['intp'].value, 
                item.attributes['mode1'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in br1_temp:
        print(
            'intp :', obj.intp,
            ', mode1 :', obj.mode1,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_br2(mydoc):
    br2 = mydoc.getElementsByTagName('BR2')
    br2_temp = []

    # menampilkan jumlah constraint BR2
    print('Jumlah BR2 : {}'.format(len(br2)))

    # simpan value tiap atribut ke array
    for item in br2:
        # print(slot.attributes['name'].value)
        br2_temp.append( 
            BR2(
                item.attributes['intp'].value, 
                item.attributes['homeMode'].value,
                item.attributes['mode2'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in br2_temp:
        print(
            'intp :', obj.intp,
            ', homeMode :', obj.homeMode,
            ', mode2 :', obj.mode2,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_fa2(mydoc):
    fa2 = mydoc.getElementsByTagName('FA2')
    fa2_temp = []

    # menampilkan jumlah constraint FA2
    print('Jumlah FA2 : {}'.format(len(fa2)))

    # simpan value tiap atribut ke array
    for item in fa2:
        # print(slot.attributes['name'].value)
        fa2_temp.append( 
            FA2(
                item.attributes['intp'].value,
                item.attributes['mode'].value,
                item.attributes['penalty'].value,
                item.attributes['slots'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in fa2_temp:
        print(
            'intp :', obj.intp,
            ', mode :', obj.mode,
            ', penalty :', obj.penalty,
            ', slots :', obj.slots,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )

def handle_se1(mydoc):
    se1 = mydoc.getElementsByTagName('SE1')
    se1_temp = []

    # menampilkan jumlah constraint SE1
    print('Jumlah SE1 : {}'.format(len(se1)))

    # simpan value tiap atribut ke array
    for item in se1:
        # print(slot.attributes['name'].value)
        se1_temp.append( 
            SE1(
                item.attributes['mode1'].value, 
                item.attributes['min'].value,
                item.attributes['penalty'].value,
                item.attributes['teams'].value,
                item.attributes['type'].value,
            ) 
        )

    for obj in se1_temp:
        print(
            'mode1 :', obj.mode1,
            ', min :', obj.min,
            ', penalty :', obj.penalty,
            ', teams :', obj.teams,
            ', type :', obj.type,
        sep =' ' )